<?php
require 'vendor/autoload.php';
use Aws\EC2\Ec2Client;

function setClient($key, $secret) {
    $ec2Client = new Aws\Ec2\Ec2Client([
        'region' => 'us-east-1',
        'version' => '2016-11-15',
        'credentials' => [
            'key'    => $key,
            'secret' => $secret,
        ],
    ]);

    return $ec2Client;
}

function testCreds($key, $secret) {
    // Use basic call to test creds
    $ec2Client = setClient($key, $secret);

    return $ec2Client->describeInstances();
}

function getdetails($key, $secret, $instanceId) {
    // Fetch details of new server to show public / private IP and DNS
    $ec2Client = setClient($key, $secret);

    $result = $ec2Client->describeInstances(array(
        'InstanceIds' => [$instanceId],
    ));

    $data = [];
    foreach ($result as $key => $value) {
        $data[$key] = $value;
    }

    return $data;

}

function createSecurityGroup($key, $secret, $securityGroupName) {
    // Create new security group for server
    $ec2Client = setClient($key, $secret);

    $result = $ec2Client->createSecurityGroup(array(
        'GroupName'   => $securityGroupName,
        'Description' => 'Snapt load balencer'
    ));

    // Set ingress rules for the security group
    $ec2Client->authorizeSecurityGroupIngress(array(
        'GroupName'     => $securityGroupName,
        'IpPermissions' => array(
            array(
                'IpProtocol' => 'tcp',
                'FromPort'   => 80,
                'ToPort'     => 80,
                'IpRanges'   => array(
                    array('CidrIp' => '0.0.0.0/0')
                ),
            ),
            array(
                'IpProtocol' => 'tcp',
                'FromPort'   => 443,
                'ToPort'     => 443,
                'IpRanges'   => array(
                    array('CidrIp' => '0.0.0.0/0')
                ),
            ),
            array(
                'IpProtocol' => 'tcp',
                'FromPort'   => 22,
                'ToPort'     => 22,
                'IpRanges'   => array(
                    array('CidrIp' => '0.0.0.0/0')
                ),
            )
        )
    ));
}

function createServer($key, $secret, $keyPairName, $securityGroupName) {
    // Create new server
    $ec2Client = setClient($key, $secret);

    createSecurityGroup($key, $secret, $securityGroupName);

    // Launch an instance with the key pair and security group
    $result = $ec2Client->runInstances(array(
        'ImageId'        => 'ami-570f603e',
        'MinCount'       => 1,
        'MaxCount'       => 1,
        'InstanceType'   => 'm1.small',
        'KeyName'        => $keyPairName,
        'SecurityGroups' => array($securityGroupName),
    ));
    $instanceIds = $result->getPath('Instances/*/InstanceId');

    // return current($result->getPath('Reservations/*/Instances/*/PublicDnsName'));
    // extract private data from object
    $data = [];
    foreach ($result as $key => $value) {
        $data[$key] = $value;
    }

    return $data;
}

function createKeyPair($key, $secret, $keyPairName) {
    // Create new key pair for SSH
    $ec2Client = setClient($key, $secret);

    // TODO: KeyFormat 'pem|ppk',
    $result = $ec2Client->createKeyPair(array(
        'KeyName' => $keyPairName,
    ));

    // extract private data from object
    $data = [];
    foreach ($result as $key => $value) {
        $data[$key] = $value;
    }

    return $data;
}

if (isset($_POST['endpoint'])) {
    $data = get_object_vars(json_decode($_POST['data']));

    switch ($_POST['endpoint']) {
        case 'createKeyPair':
            $result = createKeyPair($data['key'], $data['secret'], $data['keyPairName']);

            http_response_code( 200 );
            echo json_encode( $result );

            break;
        case 'testCreds':
            $data = get_object_vars(json_decode($_POST['data']));
            $result = testCreds($data['key'], $data['secret']);

            http_response_code( 200 );
            var_dump($result);
            echo json_encode( $result );

            break;
        case 'InstanceId':
            $data = get_object_vars(json_decode($_POST['data']));
            $result = testCreds($data['key'], $data['secret']);

            http_response_code( 200 );
            var_dump($result);
            echo json_encode( $result );

            break;
        case 'createServer':
            $data = get_object_vars(json_decode($_POST['data']));
            $result = createServer($data['key'], $data['secret'], $data['keyPairName'], $data['lbName']);

            http_response_code( 200 );
            echo json_encode( $result );

            break;
        case 'getDetails':
            $data = get_object_vars(json_decode($_POST['data']));
            $result = getdetails($data['key'], $data['secret'], $data['InstanceId']);

            http_response_code( 200 );
            echo json_encode( $result );

            break;
                
        default:
        
            break;
    }
}

