const ajaxPOST = (endpoint, data, msgObj, val = null) => {
    let formData = new FormData();
    formData.append('endpoint', endpoint);
    formData.append('data', data);

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "api.php");
    xhr.onload = function () {
        const data = JSON.parse(this.response);

        switch (val) {
            case 'InstanceId':
                msgObj.innerHTML = data.Instances[0].InstanceId;
                break;
            case 'KeyMaterial':
                msgObj.innerHTML = data.KeyMaterial;
                break;
            case 'InstanceDetails':
                let PrivateIpAddress = document.createElement( 'li' );
                PrivateIpAddress.appendChild(document.createTextNode( 'PrivateIpAddress: ' + data.Reservations[0].Instances[0].PrivateIpAddress ));
                msgObj.appendChild(PrivateIpAddress);

                let PrivateDnsName = document.createElement( 'li' );
                PrivateDnsName.appendChild(document.createTextNode( 'PrivateDnsName: ' + data.Reservations[0].Instances[0].PrivateDnsName ));
                msgObj.appendChild(PrivateDnsName);

                let PublicDnsName = document.createElement( 'li' );
                PublicDnsName.appendChild(document.createTextNode( 'PublicDnsName: ' + data.Reservations[0].Instances[0].PublicDnsName ));
                msgObj.appendChild(PublicDnsName);
                break;
            default:
                if (this.response > '') {
                    msgObj.classList.add('success');
                    msgObj.innerHTML = 'Success';
                } else {
                    msgObj.classList.add('error');
                    msgObj.innerHTML = 'Error';
                }
                break;
        }
        for (const item of document.querySelectorAll('.loader')) {
            item.classList.remove('loader');
        }

        return this.response;
    };
    xhr.send(formData);
}

const addServerToList = (event) => {
    event.preventDefault();
    const serverName = document.querySelector('#serverName');
    updateServerList(serverName.value);
    serverName.value = '';
}

const updateServerList = (serverName) => {
    let serverListBlock = document.querySelector('#serverList');
    
    serverListBlock.appendChild(makeServerItem(serverName));
}

const makeServerItem = (serverName) => {
    let item = document.createElement( 'li' );
    item.appendChild(document.createTextNode( serverName ));
 
    let close = document.createElement( 'button' );
    close.setAttribute('id', serverName);
    close.setAttribute('class', 'server_btn remove_server');
    close.appendChild(document.createTextNode( 'Remove server' ));
    close.addEventListener('click', removeServer);

    item.appendChild(close)
 
    return item;
}

const removeServer = (event) => {
    event.preventDefault();
    setTimeout(event.target.parentNode.remove(), 2200);
}

const resetFormFields = (event) => {
    event.preventDefault();
    console.log('reset');
}

const getDetails = (event) => {
    event.preventDefault();
    event.target.classList.add('loader')
    const apiKey = document.querySelector('#apiKey');
    const apiSecret = document.querySelector('#apiSecret');
    const getDetailsMsg = document.querySelector('#getDetailsMsg');
    const InstanceId = document.querySelector('#InstanceId');
    const data = JSON.stringify({
        'key': apiKey.value,
        'secret': apiSecret.value,
        'InstanceId': InstanceId.innerHTML
    });

    if (InstanceId.innerHTML != '') {
        ajaxPOST('getDetails', data, getDetailsMsg, 'InstanceDetails');
    }
}

const createLoadBalancer = async (event) => {
    event.preventDefault();
    event.target.classList.add('loader')
    let validate = true;
    const apiKey = document.querySelector('#apiKey');
    const apiSecret = document.querySelector('#apiSecret');
    const apiKeyMsg = document.querySelector('#apiKeyMsg');
    const keyPairName = document.querySelector('#sshKeyName');
    const keyPairNameMsg = document.querySelector('#keyPairNameMsg');
    const lbName = document.querySelector('#lbName');
    const InstanceId = document.querySelector('#InstanceId');

    const data = JSON.stringify({
        'key': apiKey.value,
        'secret': apiSecret.value,
        'keyPairName': keyPairName.value,
        'lbName': lbName.value
    });

    if (apiKey.value == '' || apiSecret.value == '') {
        validate = false;
        apiKey.classList.add('error');
        apiSecret.classList.add('error');
        apiKeyMsg.innerHTML = "Missing API key or secret"
    } else {
        apiKey.classList.remove('error');
        apiSecret.classList.remove('error');
        apiKeyMsg.innerHTML = ""
    }
    if (keyPairName.value == '') {
        validate = false;
        keyPairName.classList.add('error');
        keyPairNameMsg.innerHTML = "Missing SSH key pair."
    } else {
        keyPairName.classList.remove('error');
        keyPairNameMsg.innerHTML = ""
    }
    if (lbName.value == '') {
        validate = false;
        lbName.classList.add('error');
        document.querySelector('#lbNameMsg').innerHTML = "Missing load balancer."
    } else {
        lbName.classList.remove('error');
        document.querySelector('#lbNameMsg').innerHTML = ""
    }

    if (validate) {
        ajaxPOST('createServer', data, InstanceId, 'InstanceId');
    } else {
        for (const item of document.querySelectorAll('.loader')) {
            item.classList.remove('loader');
        }
    }
}

const testCreds = (event) => {
    event.preventDefault();
    event.target.classList.add('loader')
    const apiKey = document.querySelector('#apiKey');
    const apiSecret = document.querySelector('#apiSecret');
    const creds = JSON.stringify({'key': apiKey.value,'secret': apiSecret.value});
    const credStatus = document.querySelector('#credStatus');
    if (apiKey.value == '' || apiSecret.value == '') {
        validate = false;
        apiKey.classList.add('error');
        apiSecret.classList.add('error');
        apiKeyMsg.innerHTML = "Missing API key or secret"
        event.target.classList.remove('loader')
    } else {
        ajaxPOST('testCreds', creds, credStatus);
        apiKey.classList.remove('error');
        apiSecret.classList.remove('error');
        apiKeyMsg.innerHTML = ""
    }
}

const generateSSHKey = (event) => {
    event.preventDefault();
    event.target.classList.add('loader')
    const apiKey = document.querySelector('#apiKey');
    const apiSecret = document.querySelector('#apiSecret');
    const sshKey = document.querySelector('#sshKey');
    const sshKeyMsg = document.querySelector('#sshKeyMsg');
    const keyPairName = document.querySelector('#sshKeyName');
    const data = JSON.stringify({'key': apiKey.value,'secret': apiSecret.value, 'keyPairName': keyPairName.value});

    if (keyPairName.value == '') {
        keyPairName.classList.add('error');
        keyPairNameMsg.innerHTML = "Missing SSH key pair."
        event.target.classList.remove('loader')
    } else {
        ajaxPOST('createKeyPair', data, sshKey, 'KeyMaterial');
        keyPairName.classList.remove('error');
        keyPairNameMsg.innerHTML = ""
    }
}

const testCredsBtn = document.querySelector('#testCredsBtn');
testCredsBtn.addEventListener('click', testCreds);

const generateSSHBtn = document.querySelector('#generateSSHBtn');
generateSSHBtn.addEventListener('click', generateSSHKey);

let addServer = document.querySelector('#addServer');
addServer.addEventListener('click', addServerToList);

let createBtn = document.querySelector('#createBtn');
createBtn.addEventListener('click', createLoadBalancer);

let getDetailsBtn = document.querySelector('#getDetailsBtn');
getDetailsBtn.addEventListener('click', getDetails);
