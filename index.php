<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Create Snapt load balencer</title>
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
  </head>
  <body class="body">
    <header class="header">
      <div class="wrapper">
        <h1 class="logo">Create load blancer</h1>
      </div>
    </header>
    <main class="main">
      <div class="wrapper">
        <form id="createLoadBalancer" class="create_load_balancer" method="post" action="" name="createLoadBalancer" target="_parent">
          <fieldset id="providerFieldset" class="fieldset">
            <label class="label" for="provider">Select your cloud provider:</label>
            <select id="provider" class="provider" name="provider" required >
              <option>Select cloud provider</option>
              <option value="aws">AWS</option>
            </select>
            <div id="providerMsg" class="msg"></div>
          </fieldset>

          <fieldset id="apiKeyFieldset" class="fieldset">
            <label class="label" for="apiKey">Provider API key:</label>
            <input id="apiKey" class="api_key" name="apiKey" type="text" placeholder="API key" required />
            <label class="label" for="apiSecret">Provider API Secret:</label>
            <input id="apiSecret" class="api_key" name="apiSecret" type="text" placeholder="API secret" required />
            <div id="apiKeyMsg" class="msg"></div>
            <div class="test_creds_wrapper">
              <button id="testCredsBtn" class="btn test_creds">Test credentials<span></span></button>
              <div id="credStatus" class="cred_status"></div>
            </div>
          </fieldset>

          <fieldset id="sshKeyFieldset" class="fieldset">
            <label class="label" for="sshKeyName">SSH key name:</label>
            <input id="sshKeyName" class="ssh_key_name" name="sshKeyName" type="text" placeholder="SSH key name" required />
            <div id="keyPairNameMsg" class="msg"></div>
            <div class="generate_ssh_wrapper">
              <button id="generateSSHBtn" class="btn generate_ssh">Generate SSH key<span></span></button>
            </div>
            <p><strong>Your private key</strong></p>
            <pre id="sshKey" class="msg"></pre>
            <p><em>Copy and keep this key safe. Once you close this page you won't be able to get it again.</em></p>
          </fieldset>
          
          <fieldset id="protocalFieldset" class="fieldset">
            <label class="label" for="protocal">Select load balencer protocal:</label>
            <input type="radio" id="HTTP" name="protocal" value="HTTP">
            <label for="HTTP">HTTP</label>
            <input type="radio" id="HTTPS" name="protocal" value="HTTPS">
            <label for="HTTPS">HTTPS</label>
            <input type="radio" id="TCP" name="protocal" value="TCP">
            <label for="TCP">TCP</label>
            <div id="protocalMsg" class="msg"></div>
          </fieldset>

          <fieldset id="lbNameFieldset" class="fieldset">
            <label class="label" for="lbName">Load blancer name:</label>
            <input id="lbName" class="lb_name" name="lbName" type="text" placeholder="Load blancer name" required /> 
            <div id="lbNameMsg" class="msg"></div>
          </fieldset>

          <fieldset id="serverNamesFieldset" class="fieldset">
            <label class="label" for="serverName">Backend name(s):</label>
            <input id="serverName" class="server_name" name="serverName" type="text" placeholder="Backend name" /> 
            <button id="addServer" class="server_btn add_server">+</button>
            <div id="serverNameMsg" class="msg"></div>
            <ul id="serverList" class="server_list"></ul>
          </fieldset>

          <div class="ctrl_btns">
            <button id="createBtn" class="btn create_btn" type="submit" form="createLoadBalancer" value="Create">Create<span></span></button>
            <div>
              <p>Your Load Balancer instance ID</p>
              <div id="InstanceId" class="msg"></div>
            </div>
          </div>

          <fieldset id="serverDetailsFieldset" class="fieldset">
            <p>It can take a few minutes for the Load Balancer to come on line. To get the DNS and IP address details click below.</p>
            <button id="getDetailsBtn" class="btn get_details_btn">Get details<span></span></button>
            <p>Your load blancer details</p>
            <ul id="getDetailsMsg" class="msg"></ul>
          </fieldset>
        </form>
      </div>
    </main>
    <footer class="footer">
      <div class="wrapper">cuan&copy;</div>
    </footer>

    <script src="assets/js/scripts.js"></script>
  </body>
</html>
